// Proyecto: Atom

// Sesión / Usuario
/*
Actions:
-Iniciar Sesión
-Cerrar Sesión
-Insertar Información
-Actualizar Información
*/



// -------------------------------- Carrito
/*
Actions
-Añadir item
-Eliminar item
-Establecer cantidad
*/


interface Order {
    id: string,
    items: [
        {
            productId: string,
            amount: number,
            properties?: {
                color?: string,
                size?: string,
            }
        }
    ],
    deliveryAddress: User["delivery"]
} 