import path from 'path';
import webpack from 'webpack';
import HTMLWebpackPlugin from 'html-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';

export const BASE_PATH = path.resolve(__dirname, '../../');
export const BUILD_PATH = `${BASE_PATH}/build`

const webpackCommonConfig = (env: any, arg: any): webpack.Configuration => ({
    entry: `${BASE_PATH}/index.ts`,
    output: {
        path: BUILD_PATH,
        filename: `index.js`,
        clean: true
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
        alias: {
            '@reducers': `${BASE_PATH}/src/reducers/`,
            '@components': `${BASE_PATH}/src/components/`,
            '@actions': `${BASE_PATH}/src/actions/`,
            '@store': `${BASE_PATH}/src/store/`,
            '@hooks': `${BASE_PATH}/src/hooks/`
        },
    },
    module: {
        rules: [
            {
                test: [ /\.tsx?$/, /\.jsx?$/, /\.js?$/, /\.ts?$/ ],
                use: 'babel-loader',
                exclude: '/node_modules'
            },
            {
                test: [ /\.html?$/ ],
                use: 'html-loader',
                exclude: '/node_modules'
            },
            {
                test: [ /\.css?$/, /\.scss?$/ ],
                use: [ MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader' ],
                exclude: '/node-modules'
            },
            {
                test: [ /\.png?$/, /\.jpg?$/, /\.webp?$/, /\.gif?$/ ],
                use: [ 'file-loader' ],
                exclude: '/node-modules'
            },
        ]
    },
    plugins: [
        new HTMLWebpackPlugin({
            filename: './index.html',
            template: `${BASE_PATH}/public/index.html`,
            inject: true
        }),
        new MiniCssExtractPlugin({
            filename: './style.css'
        })
    ]
})

export default webpackCommonConfig;