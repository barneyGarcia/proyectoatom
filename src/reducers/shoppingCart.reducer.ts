import { ShoppingCartAction, ShoppingCartActionType, ShoppingCartState } from "../@types/reducer"

const initialState: ShoppingCartState = {
    products: [],
    totalItem: 0,
    totalPrice: 0
}

export const ShoppingCartReducer = (state: ShoppingCartState = initialState, action: ShoppingCartAction) => {
    console.log(state, action);
    switch (action.type) {

        case ShoppingCartActionType.ADD:
            return {
                ...state,
                products: [...state.products, action.payload.product],
                totalPrice: action.payload.totalPrice,
                totalItems: action.payload.totalItems
            }
        case ShoppingCartActionType.REMOVE:
            return {
                ...state,
                products: state.products.filter(product => product.id !== action.payload.id),
                totalPrice: action.payload.totalPrice,
                totalItems: action.payload.totalItems
            }
        case ShoppingCartActionType.UPDATE_QUANTITY:
            return {
                ...state,
                products: state.products.map(product => (product.id !== action.payload.id) ? {...product, quantity: action.payload.quantity} : product),
                totalPrice: action.payload.totalPrice,
                totalItems: action.payload.totalItems
            }
        default:
            return state
    }
}
