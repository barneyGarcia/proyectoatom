import { AuthActions, AuthActionType, AuthState } from "../@types/reducer"

const initialState: AuthState = {
  user: {
    id: '',
    email: '',
    identity: {
      userName: '',
      name: '',
      lastName: '',
      age: 0
    }
  },
  isLogged: false
}

export const authReducer = (state: AuthState = initialState, action: AuthActions) => {
  console.log(state, action)

  switch (action.type) {
    case AuthActionType.LOGIN:
      return {
        ...state,
        ...action.payload,
        isLogged: true
      }
    case AuthActionType.LOGOUT:
      return {
        ...initialState,
        isLogged: false
      }
      break;
    default:
      return state;
  }
}
