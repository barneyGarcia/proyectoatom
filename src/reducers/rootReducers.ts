import { combineReducers } from 'redux'
import { authReducer, ShoppingCartReducer } from './'

export const rootRecuder = combineReducers({
    auth: authReducer,
    shoppingCart: ShoppingCartReducer
})