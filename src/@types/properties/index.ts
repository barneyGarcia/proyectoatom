interface Identity {
    userName: string,
    name: string,
    lastName: string,
    age: number
}

interface Geo {
    countryId: string,
    currencyISO: string
}

interface Address {
    id: string,
    street: string,
    suburb: string,
    state: string,
    zipCode: number,
    counrty: string,
    reference?: string
}

interface Delivery {
    Adressess: Array<Address>,
    contactPhone: string
}

interface CardMethod {
    headliner: string,
    cardNumber: number,
    expDate: string
}

interface PaymentMethods {
    debitCards: Array<CardMethod>,
    creditCards: Array<CardMethod>
}

export interface Product {
    id: string,
    description: string,
    brand: string,
    price: string,
}

export interface User {
    id: string,
    email: string,
    userPhoto?: string,
    identity: Identity,
    geo?: Geo,
    delivery?: Delivery,
    paymentMethods?: PaymentMethods,
    recentlyViewed?: Array<Product>
}

export interface ShoppingItem extends Product{
    quantity: number
}

export interface ShoppingCart {
    totalItem: number,
    products: Array<ShoppingItem>,
    totalPrice: number
}