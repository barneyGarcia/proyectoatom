import { Product, ShoppingCart, ShoppingItem, User } from "../properties";

export interface AuthState {
    user: User,
    isLogged: boolean
}

export enum AuthActionType {
    LOGIN = '[AUTH] LOGIN',
    LOGOUT = '[AUTH] LOGOUT'
}

export interface AuthLoginAction {
    type: AuthActionType.LOGIN,
    payload: Omit<AuthState, 'isLogged'>
}

export interface AuthLogoutAction {
    type: AuthActionType.LOGOUT
}

export type AuthActions = AuthLoginAction | AuthLogoutAction;

export interface ShoppingCartState extends ShoppingCart {}

export enum ShoppingCartActionType {
    ADD = '[SHOPPINGCART] Add',
    REMOVE = '[SHOPPINGCART] REMOVE',
    UPDATE_QUANTITY = '[SHOPPINGCART] UPDATE_QUANTITY'
}

export interface CartAddAction {
    type: ShoppingCartActionType.ADD,
    payload: {
        product: Product,
        totalPrice: number,
        totalItems: number
    }
}

export interface CartRemoveAction {
    type: ShoppingCartActionType.REMOVE,
    payload: {
        id: string,
        totalPrice: number,
        totalItems: number
    }
}

export interface CartUpdateQuantityAction {
    type: ShoppingCartActionType.UPDATE_QUANTITY,
    payload: {
        id: string,
        quantity: number,
        totalPrice: number,
        totalItems: number
    }
}

export type ShoppingCartAction = CartAddAction | CartRemoveAction | CartUpdateQuantityAction;