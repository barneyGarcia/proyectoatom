import { useDispatch } from '@hooks/useDispatch'
import React from 'react'

export const NavBar = () => {
    const { authLoginAction, cartAddAction} = useDispatch();

    const handleLogin = () => {
        authLoginAction({
            user: {
                id: `${new Date().getTime()}`,
                email: 'barney@garcia.com',
                identity: {
                    userName: 'barneyGarcia',
                    name: 'Barney',
                    lastName: 'Garcia',
                    age: 21
                }
            }
        });
    }

    const handleAddProductToCart = () => {
        cartAddAction(
            {
                id: '187382719318',
                brand: 'Nike',
                description: 'SuperBoard Unisex',
                price: '$760',
                quantity: 1
            },
            1,
            760
        );
    }

    return (
        <div>
            <button onClick={handleLogin}>Iniciar Sesión</button>
            <button onClick={handleAddProductToCart}>Add Product to Cart</button>
        </div>
    )
}
