import { rootRecuder } from '@reducers/rootReducers'
import { createStore } from 'redux'

const initStore = () => {
    return createStore(rootRecuder);
}

export const store = initStore();
