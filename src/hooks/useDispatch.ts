import { bindActionCreators } from "redux";
import { useDispatch as dispatch } from "react-redux";
import { authLoginAction, cartAddAction } from "@actions/index";
import { store } from "@store/store"

export const useDispatch = () => {
    return bindActionCreators({
        authLoginAction,
        cartAddAction
    }, dispatch());
}