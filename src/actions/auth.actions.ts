import { AuthActionType, AuthLoginAction, AuthState } from "../@types/reducer";

export const authLoginAction = (auth: Omit<AuthState, 'isLogged'>): AuthLoginAction => ({
    type: AuthActionType.LOGIN,
    payload: auth
})