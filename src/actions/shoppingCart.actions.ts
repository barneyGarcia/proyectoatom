import { ShoppingItem } from "src/@types/properties";
import { CartAddAction, ShoppingCartActionType } from "../@types/reducer";

export const cartAddAction = (
    product: ShoppingItem,
    totalItems: number,
    totalPrice: number
): CartAddAction => ({
    type: ShoppingCartActionType.ADD,
    payload: {
        product,
        totalItems,
        totalPrice
    }
})